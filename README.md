This is a role-playing game for Android that is intended to be a love letter to 2D Japanese RPG (JRPG)s of the early 1990s. It is intended to look and feel like a game from that era, with the same kind of charm an RPG from those eras had that was lost after the release of Final Fantasy 7. Essentially, this is intended to bring many of the things that made older JRPGs great onto a more modern platform, while adapting many other positive aspects of other games made since then.

The working title for this game is Project Lavender, but that title will likely change over time. I just chose the title for now because it's a distinctive enough name to grab my attention in my inbox!

It's not the most original project to ever exist. It's not intended to change the world or anything like that, this game only exists because of how much I loved other games like this.
