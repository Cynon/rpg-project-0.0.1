package edu.luc.cwickum.rpg422.ui;

/**
 * Created by Cooper on 12/2/13.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import edu.luc.cwickum.rpg422.R;

public class StatsList extends BaseAdapter {

    Context context;
    String[] mName;
    int[] mStat;
    int[] mStatYellow;
    int[] mIcon;
    LayoutInflater inflater;

    public StatsList(Context context, int[] icon, String[] name, int[] stat, int[] yellow) {
        this.context = context;
        this.mName = name;
        this.mStat = stat;
        this.mStatYellow = yellow;
        this.mIcon = icon;
    }

    @Override
    public int getCount() {
        return mName.length;
    }

    @Override
    public Object getItem(int position) {
        return mName[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        TextView txtName;
        TextView txtStat;
        TextView txtStatYellow;
        ImageView imgIcon;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.stats_menu, parent,
                false);

        txtName = (TextView) itemView.findViewById(R.id.name);
        txtStat = (TextView) itemView.findViewById(R.id.statNow);
        txtStatYellow = (TextView) itemView.findViewById(R.id.statChange);
        imgIcon = (ImageView) itemView.findViewById(R.id.icon);

        txtName.setText(mName[position]);
        txtStat.setText(mStat[position] + "");
        txtStatYellow.setText(mStatYellow[position] + "");
        imgIcon.setImageResource(mIcon[position]);

        return itemView;
    }

}

