package edu.luc.cwickum.rpg422.entity.monster;

import android.os.Parcel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import edu.luc.cwickum.rpg422.R;
import edu.luc.cwickum.rpg422.entity.Entity;
import edu.luc.cwickum.rpg422.entity.Spell;
import edu.luc.cwickum.rpg422.item.ItemFactory;

/**
 * Created by Cooper Wickum on 10/28/13.
 */
public class Monster extends Entity {

    private ArrayList<Integer> dropList; //A list of item IDs that the monsters have.
    private HashMap<Integer, Integer> drops; //The first Integer corresponds to the ID of an item, the second to the chance of it dropping.
    private Integer xpgive; //The amount of experience to give to the party once slain. Scales with level.
    private Integer xpscale; //Amount added to XP reward per level.
    private Integer xpFF; //If scaling is turned on, acts as a fudge factor to randomize experience rewards, from 0 to whatever the value is.
    private boolean useXPscale; //Does XP scale or is it static?
    private int goldDrop; //Amount of gold dropped. If XP scaling is turned off, Gold will be a static amount. Useful for bosses.
    private ArrayList<Behavior> aiBehavior; //AI behaviors.
    private ArrayList<Spell> monsterAI; //The AI.
    private Integer monster_image; //Monster image.

    public Monster() {
        super();
        this.xpFF = 0;
        this.xpgive = 0;
        this.xpscale = 0;
        this.useXPscale = false;
        this.monster_image = R.drawable.monster;
        this.aiBehavior = new ArrayList<Behavior>();

    }

    public HashMap<Integer, Integer> getDropMap() { return drops; }

    public ArrayList<Integer> getDropList() { return dropList; }

    public boolean removeDrop(Integer itemID)
    {
        if (dropList.contains(itemID))
        {
            drops.remove(itemID);
            dropList.remove(itemID);
            return true;
        }
        else return false;
    }

    public boolean setDrops(Integer itemID, Integer dropChance) {
        if (ItemFactory.getItem(itemID) != null && dropChance != null && dropChance < 101 && dropChance > 0)
        {
            this.drops.put(itemID, dropChance);
            this.dropList.add(itemID);
            return true;
        }
        else
        {
            System.err.println("Invalid item/dropChance pair: " + itemID + "/" + dropChance + ".");
            return false;
        }
    }

    public Integer getXpgive() {
        return xpgive;
    }

    public void setXPgive(Integer xpgive) {
        this.xpgive = xpgive;
    }

    public Integer getXPscale() {
        return xpscale;
    }

    public void setXPscale(Integer xpscale) {
        this.xpscale = xpscale;
    }

    public boolean isUseXPscale() {
        return useXPscale;
    }

    public void setUseXPscale(boolean useXPscale) {
        this.useXPscale = useXPscale;
    }

    //XP Fudge Factor
    public Integer getXPFF() {
        return xpFF;
    }

    //XP Fudge Factor
    public void setXPFF(Integer xpFF) {
        this.xpFF = xpFF;
    }

    public ArrayList<Behavior> getAiBehavior() {
        return aiBehavior;
    }

    public void setAiBehavior(ArrayList<Behavior> aiBehavior) {
        this.aiBehavior = aiBehavior;
    }

    public Integer getMonster_image() {
        return monster_image;
    }

    public void setMonster_image(Integer monster_image) {
        this.monster_image = monster_image;
    }

    public int getGoldDrop() { return goldDrop; }

    public boolean setGoldDrop(int g) {

        if (g >= 0)
        {
            goldDrop = g;
            return true;
        }
        else return false;

    }

    public Spell monsterAIaction() {

        //This is how the AI works. If there are 3 spells that the Monster has,
        //Spell AA with Priority 10, Spell BB with Priority 3, and Spell CC with
        //priority 1, it will add 10 instances of AA to the list, 3 BB spells,
        //and 1 CC spell. Then, a random number between 0 and the size of the list
        //will be chosen.

        monsterAI = new ArrayList<Spell>();
        int a = 0;
        while (a < this.aiBehavior.size())
        {
            int b = 0;
            while (b < aiBehavior.get(a).getPriority())
            {
                monsterAI.add(aiBehavior.get(a).getSpell());
                b++;
            }
            a++;
        }

        Random r = new Random();
        int aiSelect = r.nextInt(monsterAI.size());
        System.out.println(aiSelect + " AI " + monsterAI.get(aiSelect).getName());
        return monsterAI.get(aiSelect);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
