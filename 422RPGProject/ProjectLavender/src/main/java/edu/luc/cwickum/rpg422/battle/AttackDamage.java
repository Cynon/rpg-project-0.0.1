package edu.luc.cwickum.rpg422.battle;

import java.util.Random;

import edu.luc.cwickum.rpg422.entity.Entity;

/**
 * Created by Cooper Wickum on 12/1/13.
 *
 * This is based off of the formula used for the
 * RPG Maker 2000 game Kindred Saga, released in 2001.
 *
 * Apologies if this is not easy to read -- the original wasn't either.
 *
 */
public class AttackDamage {

    public static AttackMessage AttackDamageFormula(Entity caster, Entity target)
    {
        //Random number generator seed. Yay.
        int RNGseed = (caster.getStats().get("luck")
                * target.getStats().get("luck")
                -  target.getStats().get("defense"))
                * caster.getLevel()
                *  caster.getStats().get("attack")
                *   target.getStats().get("defense");

        System.err.println("Seed:" + RNGseed);

        Random r = new Random(RNGseed);

        boolean crit = false;

        int ad = caster.getAttackPower();
        int formX = ad / 5;
        int formY = (caster.getLevel() / 3);

        int accuracy = 85;

        int hitChance = r.nextInt(100) + 1;

        if ( attackHit(caster, target, hitChance, accuracy) )
        {
            if (hitChance <= caster.getStats().get("criticalChance"))
            {
                formY = formY + 2;
                crit = true;
            }

            while (formY != 0)
            {
               ad = ad + (r.nextInt(6) + 4);

               if (formY < 0) { formY++; ad = ad - formX;  }
               if (formY > 0) { formY--; ad = ad + formX;  }

            }

            ad = ad - target.getDefense();
            ad = ad + (r.nextInt(6) - 2);
            ad = (int) (ad * target.checkResistance(caster.getNormalAttackTypes()));

            int bmax = setBmax(ad);

            if (ad < 0) {
                ad = 0;
            }

            if (bmax > 0)
            {
                ad = ad + (r.nextInt(bmax) - (bmax/2));
            }

            caster.setBattleTicks((r.nextInt(5) + 5) * -1);
            return new AttackMessage(ad, crit, false);

        }


        else {
            caster.setBattleTicks(0);
            return new AttackMessage(0, false, true);
        }
    }

    private static int setBmax(int ad) {

        if (ad >= 1000)
        {
            return 500;
        }
        else if (ad >= 500)
        {
            return 240;
        }
        else if (ad >= 250)
        {
            return 120;
        }
        else return 0;

    }

    public static boolean attackHit(Entity caster, Entity target, int hitChance, int accuracy)
    {

        //The bad status "Dark" reduces accuracy quite significantly. It's effectively blindness.
        if (caster.getBadStatusMap().containsKey("dark"))
        {

            if (hitChance + (target.getSpeed() - caster.getSpeed()) > accuracy )
            {
                return true;
            }
            else return false;

        }
        else
        {
            if (hitChance + (target.getSpeed() - caster.getSpeed()) < accuracy )
            {
                return true;
            }
            else return false;
        }


    }

}
