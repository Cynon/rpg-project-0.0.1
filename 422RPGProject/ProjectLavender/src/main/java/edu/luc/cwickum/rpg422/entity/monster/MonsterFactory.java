package edu.luc.cwickum.rpg422.entity.monster;

import java.util.ArrayList;
import java.util.HashMap;

import edu.luc.cwickum.rpg422.R;
import edu.luc.cwickum.rpg422.entity.Spell;
import edu.luc.cwickum.rpg422.entity.SpellFactory;
import edu.luc.cwickum.rpg422.item.Item;
import edu.luc.cwickum.rpg422.item.ItemConsumable;
import edu.luc.cwickum.rpg422.item.ItemFactory;

/**
 * Created by Cooper Wickum on 11/5/13.
 */

public class MonsterFactory {

    public static Monster getMonster(Integer i)
    {

        if (i < 0) { return null; }

        else if (i == 10) {
            Monster m = new Monster();
            m.setName("BalloonGhost");

            Integer hp = 35;
            Integer mp = 5;
            Integer attack = 13;
            Integer defense = 5;
            Integer magicPower = 5;
            Integer magicRes = 2;
            Integer speed = 13;
            Integer luck = 1;
            Integer critChance = 5;

            m.setStats(hp, mp, attack, defense, magicPower, magicRes, speed, luck);
            m.setGrowthRate(5, 2, 3, 3, 2, 1, 2, 1);

            m.setXPgive(10);
            m.setXPFF(2);
            m.setUseXPscale(true);

            ArrayList<Behavior> behaviors = new ArrayList<Behavior>();
            behaviors.add(new Behavior(50, SpellFactory.getSpell(0))); //Regular attack

            m.setAiBehavior(behaviors);

            m.setGoldDrop(30);
            m.setDrops(20, 50);

            m.setMonster_image(R.drawable.monster_redballoonghost);

            m.setHealth(hp);
            return m;
        }

        else if (i == 11) {
            Monster m = new Monster();
            m.setName("BlueHood");

            Integer hp = 110;
            Integer mp = 10;
            Integer attack = 43;
            Integer defense = 10;
            Integer magicPower = 7;
            Integer magicRes = 3;
            Integer speed = 22;
            Integer luck = 2;
            Integer critChance = 5;

            m.setStats(hp, mp, attack, defense, magicPower, magicRes, speed, luck);
            m.setGrowthRate(5, 2, 3, 3, 2, 1, 2, 1);

            m.setXPgive(10);
            m.setXPFF(2);
            m.setUseXPscale(true);

            ArrayList<Behavior> behaviors = new ArrayList<Behavior>();
            behaviors.add(new Behavior(50, SpellFactory.getSpell(0))); //Regular attack

            m.setAiBehavior(behaviors);

            m.setGoldDrop(40);
            m.setDrops(20, 40);

            m.setMonster_image(R.drawable.monster_blueballoonghost);

            m.setHealth(hp);
            return m;
        }

        else if (i == 1100) {
            Monster m = new Monster();
            m.setName("Warlock");

            Integer hp = 250;
            Integer mp = 150;
            Integer attack = 25;
            Integer defense = 10;
            Integer magicPower = 15;
            Integer magicRes = 10;
            Integer speed = 10;
            Integer luck = 5;
            Integer critChance = 10;

            m.setStats(hp, mp, attack, defense, magicPower, magicRes, speed, luck);
            m.setGrowthRate(10, 5, 3, 3, 2, 1, 2, 1);
            m.setCritChance(critChance);

            m.setXPgive(1000);
            m.setXPFF(0);
            m.setUseXPscale(true);

            ArrayList<Behavior> behaviors = new ArrayList<Behavior>();
            behaviors.add(new Behavior(50, SpellFactory.getSpell(0))); //Regular attack

            m.setAiBehavior(behaviors);

            m.setGoldDrop(30);
            m.setDrops(20, 50);

            m.setHealth(hp);
            return m;
        }



        return null;
    }


}
