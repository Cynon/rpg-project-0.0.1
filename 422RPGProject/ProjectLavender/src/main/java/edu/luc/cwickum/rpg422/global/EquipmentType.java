package edu.luc.cwickum.rpg422.global;

/**
 * Created by Cooper Wickum on 11/19/13.
 */

//Equipment types.
public enum EquipmentType {
    armor, boots, relic
}
