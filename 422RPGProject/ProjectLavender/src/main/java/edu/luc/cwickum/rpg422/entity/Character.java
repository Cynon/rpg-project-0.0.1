package edu.luc.cwickum.rpg422.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import edu.luc.cwickum.rpg422.item.*;

/**
 * Created by Cooper Wickum on 10/29/13.
 */
public class Character extends Entity {

    private Integer xp; //Experience Points
    private ItemWeapon weapon;
    private ItemEquip armor;
    private ItemEquip esper;
    private ItemEquip boots;
    private ArrayList<Spell> spellBook;

    public Character() {
        super();

        xp = 0;


    }

    public Character(Parcel in) {

        readFromParcel(in);

    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeInt(xp);
        parcel.writeList(spellBook);

    }

    @Override
    public void readFromParcel(Parcel in)
    {
        super.readFromParcel(in);

        xp = in.readInt();
        spellBook = in.readArrayList(Character.class.getClassLoader());
    }


    public static final Parcelable.Creator<Character> CREATOR
            = new Parcelable.Creator<Character>() {
        public Character createFromParcel(Parcel in) {
            return new Character(in);
        }

        public Character[] newArray(int size) {
            return new Character[size];
        }
    };



}
