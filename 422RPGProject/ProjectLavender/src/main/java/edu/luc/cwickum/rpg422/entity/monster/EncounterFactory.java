package edu.luc.cwickum.rpg422.entity.monster;

import java.util.ArrayList;

import edu.luc.cwickum.rpg422.entity.SpellFactory;
import edu.luc.cwickum.rpg422.item.Item;
import edu.luc.cwickum.rpg422.item.ItemConsumable;

/**
 * Created by Cooper Wickum on 11/5/13.
 */

public class EncounterFactory {


public static ArrayList<Monster> encounterMonster(Integer i)
{

    if (i < 0) { return null; }
    else if (i == 10) {
        ArrayList<Monster> encounter = new ArrayList<Monster>();

        encounter.add(MonsterFactory.getMonster(10));
        encounter.add(MonsterFactory.getMonster(10));
        encounter.add(MonsterFactory.getMonster(11));

        return encounter;

    }



        return null;
}

}