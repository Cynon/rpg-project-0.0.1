package edu.luc.cwickum.rpg422.battle;

/**
 * Created by Cooper on 12/1/13.
 */
public class AttackMessage {

    public Integer damage;
    public boolean critical;
    public boolean miss;

    public AttackMessage(Integer dmg, boolean crit, boolean miss)
    {
        this.damage = dmg;
        this.critical = crit;
        this.miss = miss;
    }

    public boolean isMiss() {
        return miss;
    }

    public boolean isCritical() {
        return critical;
    }

    public Integer getDamage() {
        return damage;
    }
}
