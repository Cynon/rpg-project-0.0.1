package edu.luc.cwickum.rpg422.global;

/**
 * Created by Cooper Wickum on 11/19/13.
 */

//All of these are elemental damage types. The last 4 are intended for physical attacks only.
public enum DamageTypes {
    fire, ice, lightning, water, earth, wind, light, dark, slashing, crushing, piercing, ranged
}
