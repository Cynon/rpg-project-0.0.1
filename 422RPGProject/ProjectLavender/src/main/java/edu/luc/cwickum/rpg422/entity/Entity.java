package edu.luc.cwickum.rpg422.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import edu.luc.cwickum.rpg422.global.DamageTypes;

/**
 * Created by Cooper Wickum on 10/28/13.
 */
public abstract class Entity implements Parcelable {

    protected String name;
    protected ArrayList<Spell> spells;
    protected ArrayList<String> normalAttackTypes;
    protected HashMap<String, Integer> stats;
    protected HashMap<String, Integer> growthrate; //Growth rate for levelling up.
    protected HashMap<String, Double> elementRes; //Used for elemental resistances and weaknesses... if type is not here, then it will be assumed that the character is neither weak or vulnerable to the element.
    protected Integer health;
    protected Integer mana;
    protected boolean isAlive;
    protected Integer level;
    protected HashMap<String, Integer> badStatusMap;
    protected Integer battleTicks; //Used in battle for turn order.

    public Entity() {
        name = "NONAME";
        isAlive = true;
        level = 1;
        health = 1;
        mana = 0;
        battleTicks = 0;

        stats = new HashMap<String, Integer>();
        setStats(health, mana, 0, 0, 0, 0, 0, 0);
        setCritChance(0);

        growthrate = new HashMap<String, Integer>();
        setGrowthRate(0, 0, 0, 0, 0, 0, 0, 0);

        badStatusMap = new HashMap<String, Integer>();

        normalAttackTypes = new ArrayList<String>();

    }

    public Entity(Parcel in) {
        name = "NONAME";
        isAlive = true;
        stats = new HashMap<String, Integer>();
        growthrate = new HashMap<String, Integer>();
        badStatusMap = new HashMap<String, Integer>();
        normalAttackTypes = new ArrayList<String>();

        readFromParcel(in);

    }

    //Set the primary stats of an entity.
    public void setStats(Integer hp, Integer mp, Integer ad, Integer df, Integer mg, Integer mr, Integer sp, Integer lk)
    {
        hp--; notBelowZero(hp); hp++;
        stats.put("maxHP", hp);
        stats.put("maxMP", notBelowZero(mp));
        stats.put("attack", notBelowZero(ad));
        stats.put("defense", notBelowZero(df));
        stats.put("magic", notBelowZero(mg));
        stats.put("magicRes", notBelowZero(mr));
        stats.put("speed", notBelowZero(sp));
        stats.put("luck", notBelowZero(lk));
    }

    public boolean setCritChance(Integer ct)
    {

        if (getStats() != null)
        {
            stats.put("criticalChance", notBelowZero(ct));
            return true;
        }
        else return false;

    }



    public int notBelowZero(int i) {
        if (i >= 0) {
            return i;
        }
        else return 0;
    }

    //Set the entity's growth rate -- the rate stats increase after levelling up.
    public void setGrowthRate(Integer hp, Integer mp, Integer ad, Integer df, Integer mg, Integer mr, Integer sp, Integer lk)
    {
        growthrate.put("maxHP", hp);
        growthrate.put("maxMP", mp);
        growthrate.put("attack", ad);
        growthrate.put("defense", df);
        growthrate.put("magic", mg);
        growthrate.put("magicRes", mr);
        growthrate.put("speed", sp);
        growthrate.put("luck", lk);
    }

    public int hit(Spell spell)
    {
        return 0;
    }

    public boolean setName(String name) {
        this.name = name;
        return true;
    }

    public String getName()
    {
        return this.name;
    }

    public void checkMaxHP()
    {
        if (health > stats.get("maxHP"))
        {
            health = stats.get("maxHP");
        }
        else if (health <= 0)
        {
            health = 0;
            isAlive = false;
        }
        else
        {
            isAlive = true;
        }
    }

    public void checkMaxMP()
    {
        if (mana > stats.get("maxMP"))
        {
            mana = stats.get("maxMP");
        }
        if (mana <= 0)
        {
            mana = 0;
        }
    }

    public Integer getHealth() {
        return health;
    }

    public void setHealth(Integer health) {
        this.health = health;
        checkMaxHP();
    }

    public int hit(Integer dmg) {
        setHealth(this.health - dmg);
        return this.health;
    }

    public HashMap<String, Integer> getStats() {
        return stats;
    }

    public void setStats(HashMap<String, Integer> stats) {
        this.stats = stats;
    }

    public Integer getLevel() {
        return level;
    }

    private void setLevel(Integer level) {
        this.level = level;
    }

    public void levelUp() {

        Random r = new Random();

        level++;

        System.out.println(stats.get("maxHP"));

        Integer hp = stats.get("maxHP") + r.nextInt(growthrate.get("maxHP"));
        Integer mp = stats.get("maxMP") + r.nextInt(growthrate.get("maxMP"));
        Integer ad = stats.get("attack") + r.nextInt(growthrate.get("attack"));
        Integer df = stats.get("defense") + r.nextInt(growthrate.get("defense"));
        Integer mg = stats.get("magic") + r.nextInt(growthrate.get("magic"));
        Integer mr = stats.get("magicRes") + r.nextInt(growthrate.get("magicRes"));
        Integer sp = stats.get("speed") + r.nextInt(growthrate.get("speed"));
        Integer lk = stats.get("luck") + r.nextInt(growthrate.get("luck"));

        stats.put("maxHP", hp);
        stats.put("maxMP", mp);
        stats.put("attack", ad);
        stats.put("defense", df);
        stats.put("magic", mg);
        stats.put("magicRes", mr);
        stats.put("speed", sp);
        stats.put("luck", lk);

        System.out.println(stats.get("maxHP"));

    }

    public Integer getMana() {
        return mana;
    }

    public void setMana(Integer mana) {
        this.mana = mana;
        checkMaxMP();
    }

    public ArrayList<Spell> getSpells() {
        return spells;
    }

    public void setSpells(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    public HashMap<String, Integer> getBadStatusMap() {
        return badStatusMap;
    }

    public void setBadStatusMap(HashMap<String, Integer> badStatusMap) {
        this.badStatusMap = badStatusMap;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public HashMap<String, Double> getElementRes() {
        return elementRes;
    }

    public void setElementRes(String s, Double d) {

        //Weaknesses/Resistances.
        //For the double value, 1 = normal damage, 0 = no damage, 2 = double damage
        //-1 = has reverse effect, only works for magic damage types, though.

        s = DamageTypes.valueOf(s).toString();

        if (s.equalsIgnoreCase("slashing") ||
            s.equalsIgnoreCase("crushing") ||
            s.equalsIgnoreCase("piercing") ||
            s.equalsIgnoreCase("ranged"))
        {
          if (d < 0) { d = 0.00; }
        }

        elementRes.put(s, d);

    }

    public double checkResistance(ArrayList<String> types)
    {
        int a = 0;
        double resistanceAmount = 1;
        while (a < types.size())
        {
            if (elementRes.containsKey(types.get(a)))
            {
                resistanceAmount = resistanceAmount + elementRes.get(types.get(a));
            }
            else
            {
                resistanceAmount++;
            }

           a++;
        }

        if (a != 0)
        {
            return resistanceAmount / a;
        }
        else
            return 1;
    }

    public ArrayList<String> getNormalAttackTypes() {
        return normalAttackTypes;
    }

    public void setNormalAttackType(String s) {

        s = DamageTypes.valueOf(s).toString();
        normalAttackTypes.add(s);

    }

    public void clearNormalAttackType()
    {
        normalAttackTypes = new ArrayList<String>();
    }

    //battleTicks are used for determining turn order in battle.

    public Integer getBattleTicks() {
        return battleTicks;
    }

    public void setBattleTicks(Integer battleTicks) {
        this.battleTicks = battleTicks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(name);
        parcel.writeByte((byte) (isAlive ? 1 : 0));
        parcel.writeInt(level);
        parcel.writeInt(health);
        parcel.writeInt(mana);
        parcel.writeMap(stats);
        parcel.writeMap(growthrate);
        parcel.writeMap(badStatusMap);
        parcel.writeList(normalAttackTypes);

    }

    public void readFromParcel(Parcel in) {

        name = in.readString();
        isAlive = in.readByte() != 0;
        level = in.readInt();
        health = in.readInt();
        mana = in.readInt();
        stats = in.readHashMap(Entity.class.getClassLoader());
        growthrate = in.readHashMap(Entity.class.getClassLoader());
        badStatusMap = in.readHashMap(Entity.class.getClassLoader());
        normalAttackTypes = in.readArrayList(Entity.class.getClassLoader());

    }


    //Subclasses of Entity can use these next five methods for adding the additional bonuses from equipment to these.
    public int getAttackPower()
    {
        return this.getStats().get("attack");
    }
    public int getDefense()
    {
        return this.getStats().get("defense");
    }
    public int getMagicPower()
    {
        return this.getStats().get("magic");
    }
    public int getMagicDefense()
    {
        return this.getStats().get("magicRes");
    }
    public int getSpeed()
    {
        return this.getStats().get("speed");
    }

    //Easier way to deal damage.
    public void hit(int damage)
    {
        setHealth(getHealth() - damage);
    }


}
