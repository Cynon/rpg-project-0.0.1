package edu.luc.cwickum.rpg422.battle;

import java.util.Random;

import edu.luc.cwickum.rpg422.entity.Entity;
import edu.luc.cwickum.rpg422.entity.Spell;

/**
 * Created by Cooper Wickum on 12/1/13.
 *
 * This is based off of the formula used for the
 * RPG Maker 2000 game Kindred Saga, released in 2001.
 *
 * Apologies if this is not easy to read -- the original wasn't either.
 *
 * Given that this is far more advanced than Kindred Saga,
 * several modifications were made.
 *
 */
public class MagicDamage {

    public static AttackMessage MagicDamageFormula(Entity caster, Entity target, Spell spell)
    {

        //Random number generator seed. Yay.
        int RNGseed = (caster.getStats().get("luck") * target.getStats().get("luck") - target.getStats().get("magicRes"))
                * caster.getLevel() * caster.getStats().get("magic") * target.getStats().get("magicRes");

        System.err.println("Seed:" + RNGseed);

        Random r = new Random(RNGseed);

        int accuracy = spell.getAccuracy();
        int hitChance = r.nextInt(100) + 1;

        //Unique spell IDs -- used for spells that do specific things, such as
        //spells that remove percentages of the target's life.


        //REMEMBER! Spell ID 0 refers to the monster's normal attacks.
        if (spell.getId() == 0)
        {
            return AttackDamage.AttackDamageFormula(caster, target);
        }


        //True damage spells deal whatever the base damage is with no modifiers applied.
        else if (spell.isTrueDamage())
        {
            if ( attackHit(caster, target, hitChance, accuracy) )
            {
                int sbd = spell.getBaseDamage();
                if (spell.isHealing())
                {
                    sbd = sbd * -1;
                }
                return new AttackMessage(sbd, false, false);
            }
            else return new AttackMessage(0, false, true);

        }

        //md for MagicDamage, targetRes for determining the type of defense, and the
        //formX value for determining the amount to be increased (or decreased) with
        //each iteration of formY.

        int md, targetRes, formX;

        if (spell.isMagic())
        {
            md = caster.getStats().get("magic");
            targetRes = target.getStats().get("magicRes");
            formX = md / 4;
        }
        else
        {
            md = caster.getStats().get("attack");
            targetRes = target.getStats().get("defense");
            formX = md / 5;
        }

        //int formY = (caster.getLevel() / 2);
        int formY = spell.getBaseDamage() + (caster.getLevel() / 2);

        if ( attackHit(caster, target, hitChance, accuracy) )
        {

            while (formY != 0)
            {
               md = md + (r.nextInt(6) + 6);

               if (formY < 0) { formY++; md = md - formX;  }
               if (formY > 0) { formY--; md = md + formX;  }

            }

            md = md - targetRes;
            md = md + (r.nextInt(6) - 2);
            md = (int) (md * target.checkResistance(spell.getElements()));

            int bmax = setBmax(md);

            if (bmax > 0)
            {
                md = md + (r.nextInt(bmax) - (bmax/2));
            }


            if (spell.isHealing())
            {
                md = md * -1;
            }

            return new AttackMessage(md, false, false);

        }

        else return new AttackMessage(0, false, true);
    }

    private static int setBmax(int ad) {

        if (ad >= 1000)
        {
            return 500;
        }
        else if (ad >= 500)
        {
            return 240;
        }
        else if (ad >= 250)
        {
            return 120;
        }
        else return 0;

    }

    public static boolean attackHit(Entity caster, Entity target, int hitChance, int accuracy)
    {

        //The bad status "Silence" prevents magic use.
        if (caster.getBadStatusMap().containsKey("silence"))
        {
            return false;
        }
        else
        {
            if (hitChance <= accuracy )
            {
                return true;
            }
            else return false;
        }

    }

}
