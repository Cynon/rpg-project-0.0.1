package edu.luc.cwickum.rpg422.ui;

/**
 * Created by Cooper on 12/2/13.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import edu.luc.cwickum.rpg422.R;

public class BattleHeroList extends BaseAdapter {

    Context context;
    String[] mName;
    int[] mHP;
    int[] mMP;
    int[] mIcon;
    LayoutInflater inflater;

    public BattleHeroList(Context context, int[] icon, String[] name, int[] hp, int[] mp) {
        this.context = context;
        this.mName = name;
        this.mHP = hp;
        this.mMP = mp;
        this.mIcon = icon;
    }

    @Override
    public int getCount() {
        return mName.length;
    }

    @Override
    public Object getItem(int position) {
        return mName[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        TextView txtName;
        TextView txtHP;
        TextView txtMP;
        ImageView imgIcon;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.battle_hero_list, parent,
                false);

        txtName = (TextView) itemView.findViewById(R.id.charName);
        txtHP = (TextView) itemView.findViewById(R.id.charHP);
        txtMP = (TextView) itemView.findViewById(R.id.charMP);
        imgIcon = (ImageView) itemView.findViewById(R.id.icon);

        txtName.setText(mName[position]);
        txtHP.setText(mHP[position] + "");
        txtMP.setText(mMP[position] + "");
        imgIcon.setImageResource(mIcon[position]);

        return itemView;
    }

}

