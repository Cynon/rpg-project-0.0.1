package edu.luc.cwickum.rpg422.item;

/**
 * Created by Cooper on 11/5/13.
 */
public abstract class Item {

    //ItemEquip class refers to equipment, not consumables.
    protected Integer id; //Item ID. If below 0, cannot be obtained by the player.
    protected String name; //Item name
    protected String desc; //Short description.
    protected Integer price; //Base price for item. If 0, item cannot be bought or sold.
    protected boolean consumable; //Item is consumable.

    public Item() {

        id = -1;
        name = "ITEMNAME";
        desc = "...";
        price = 0;
        consumable = false;

    }

    public Item(Integer idd, String nam, String des, Integer pri)
    {
        id = idd;
        name = nam;
        desc = des;
        price = pri;
        consumable = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
