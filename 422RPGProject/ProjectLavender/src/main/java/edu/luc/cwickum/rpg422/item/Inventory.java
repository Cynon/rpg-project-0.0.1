package edu.luc.cwickum.rpg422.item;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Cooper on 11/5/13.
 *
 * Not implemented yet. Intended to be used for the party's inventory.
 * Anyone wishing to contribute should email me at cynon777@gmail.com
 * with the subject line as "Project Lavender". I will be more than happy to work with you.
 *
 */

public class Inventory implements Parcelable {

    protected ArrayList<Item> items;

    /*
    Sort all items in the Inventory by the item ID number.
     */


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }


}
