package edu.luc.cwickum.rpg422.entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

import edu.luc.cwickum.rpg422.item.Inventory;

/**
 * Created by Cooper Wickum on 11/5/13.
 */
public class Party implements Parcelable {

    private ArrayList<Character> partyMembers;
    private HashMap<String, Character> partymap;
    private Integer gold; //money
    private Inventory inv;

    public Party(ArrayList<Character> partym) {

        partyMembers = partym;
        partymap = new HashMap<String, Character>();
        gold = 0;

    }


    //Using Parcelable is CRITICAL with this class ... it allows it to be passed
    //between Activities.
    public Party(Parcel in) {
        partyMembers = new ArrayList<Character>();

        readFromParcel(in);

    }


    public boolean partyAdd(Character c)
    {
        if (partyMembers.size() <= 2)
        {
            if (getCharByName(c.getName()) == null) {
                partyMembers.add(c);
                return true;
            }
            else return false;
        }
        else return false;
    }

    public Character getCharByName(String s) {

        int a = 0;
        while (a < partyMembers.size())
        {
            if (partyMembers.get(a).getName().equals(s)) {
                return partyMembers.get(a);
            }
            a++;
        }
        return null;
    }

    public Character getCharBySlot(int a)
    {
        return this.partyMembers.get(a);
    }

    public boolean partyRem(Character c)
    {

        if (partyMembers.size() > 1)
        {
            int a = 0;
            while (a < partyMembers.size())
            {
                if (partyMembers.get(a).equals(c)) {
                    partyMembers.remove(a);
                    return true;
                }
                a++;
            }

        }
        return false;
    }

    //Used for determining how the turn order works in battle -- may be used for other
    //things as well.
    public Integer getHighestSpeed() {

        int a = 0;
        int fastest = 1;
        while (a < partyMembers.size())
        {
            if (partyMembers.get(a).getSpeed() > fastest)
            {
                fastest = partyMembers.get(a).getSpeed();
            }
            a++;
        }

        return fastest;

    }

    public int size()
    {
        return partyMembers.size();
    }

    public void fullHealAll()
    {
        int a = 0;
        while (a < partyMembers.size())
        {
            partyMembers.get(a).setHealth(partyMembers.get(a).getStats().get("maxHP"));
            partyMembers.get(a).setMana(partyMembers.get(a).getStats().get("maxMP"));
            a++;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeList( partyMembers);
        //parcel.writeMap(partymap);
        parcel.writeInt(gold);
        //parcel.write
        //private Inventory inv;

    }

    private void readFromParcel(Parcel in) {

        partyMembers = new ArrayList<Character>();
        in.readList(partyMembers, Party.class.getClassLoader());
        gold = in.readInt();

    }

    public static final Parcelable.Creator<Party> CREATOR
            = new Parcelable.Creator<Party>() {
        public Party createFromParcel(Parcel in) {
            return new Party(in);
        }

        public Party[] newArray(int size) {
            return new Party[size];
        }
    };

}

