package edu.luc.cwickum.rpg422.global;

/**
 * Created by Cooper Wickum on 11/19/13.
 */

public class DefaultCharacterNames{

    public static String getName(int a)
    {
        String s = "";
        if (a == 0) { s = "Cyzer"; }
        if (a == 1) { s = "Esche"; }
        if (a == 2) { s = "Sagan"; }

        return s;
    }
}
