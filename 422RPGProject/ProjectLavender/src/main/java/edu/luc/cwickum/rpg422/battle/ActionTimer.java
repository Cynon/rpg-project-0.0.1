package edu.luc.cwickum.rpg422.battle;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import edu.luc.cwickum.rpg422.entity.Entity;
import edu.luc.cwickum.rpg422.entity.Party;
import edu.luc.cwickum.rpg422.entity.monster.Monster;

/**
 * Created by Cooper on 12/10/13.
 *
 * This is what decides turn order, named it after the concept of the
 * "Active Time Bar" seen in Final Fantasy 6.
 *
 */
public class ActionTimer {

    private int tickMax;
    private Party mParty;
    private ArrayList<Monster> mEnemy;
    ArrayList<Entity> readyToAct;
    ArrayBlockingQueue<Entity> turnOrder;


    public ActionTimer(Party mParty, ArrayList<Monster> mEnemy)
    {

        this.mParty = mParty;
        this.mEnemy = mEnemy;
        tickMax = getFastestInBattle(mParty.getHighestSpeed()) * 2;
        readyToAct = new ArrayList<Entity>();
        //turnOrder = new ArrayBlockingQueue<Entity>();

    }

    //The same as Party.getHighestSpeed(), essentially.
    private int getFastestInBattle(int fastest) {

        int a = 0;
        while (a < mEnemy.size())
        {
            if (mEnemy.get(a).getSpeed() > fastest)
            {
                fastest = mEnemy.get(a).getSpeed();
            }
            a++;
        }

        return fastest;

    }

    //Selects which entity gets to attack next.
    public Entity nextAction()
    {
        int a = 0;
        int fasterEntity = 0;

        if (readyToAct.size() > 0)
        {

            while (a < readyToAct.size())
            {
                if (readyToAct.get(fasterEntity).getSpeed() < readyToAct.get(a).getSpeed())
                {
                  fasterEntity = a;
                }
                a++;
            }


            int aa = 0;
            while (aa < readyToAct.size()) { System.out.println(readyToAct.get(aa).getName() + " SLOT " + aa); aa++; }

            Entity e = readyToAct.get(fasterEntity);
            System.out.println(readyToAct.get(fasterEntity).getName() + " SHOULD ACT NEXT");
            readyToAct.remove(fasterEntity);
            return e;
        }
        else {
            return null;
        }
    }


    //This could have been a lot neater and smaller, but I left it as is because
    //I did not feel I had sufficient time to really make it nicer.

    //Essentially, addTick() is used to determine turn order.

    public void addTick()
    {
        int a = 0;
        while (a < mParty.size())
        {
            if (mParty.getCharBySlot(a).isAlive())
            {

                System.out.println(mParty.getCharBySlot(a).getName() + " at: " + mParty.getCharBySlot(a).getBattleTicks()
                        + "/" + tickMax);

                mParty.getCharBySlot(a).setBattleTicks(mParty.getCharBySlot(a).getBattleTicks() + mParty.getCharBySlot(a).getSpeed());

                if (mParty.getCharBySlot(a).getBattleTicks() > tickMax)
                {
                    mParty.getCharBySlot(a).setBattleTicks(tickMax);
                    readyToAct.add(mParty.getCharBySlot(a));

                    /*
                    if (!readyToAct.contains(mParty.getCharBySlot(a)))
                    {
                        readyToAct.add(mParty.getCharBySlot(a));
                        System.out.println(mParty.getCharBySlot(a).getName() + " is ready to act.");
                        System.out.println(readyToAct.get(readyToAct.size() - 1).getName() + " is last in.");
                        int aa = 0;
                         while (aa < readyToAct.size()) { System.out.println(readyToAct.get(aa).getName() + " SLOT " + aa); aa++; }
                    }
                    */
                    //if (turnOrder.contains())
                }

            }

            a++;
        }
        a = 0;
        while (a < mEnemy.size())
        {
            if (mEnemy.get(a).isAlive())
            {
                if (!readyToAct.contains(mEnemy.get(a)))
                {
                    mEnemy.get(a).setBattleTicks(mEnemy.get(a).getBattleTicks() + mEnemy.get(a).getSpeed());
                    if (mEnemy.get(a).getBattleTicks() > tickMax)
                    {
                        mEnemy.get(a).setBattleTicks(tickMax);
                        readyToAct.add(mEnemy.get(a));
                    }
                }
            }

            a++;

        }

    }

    public ArrayList<Entity> getActive() {
        return readyToAct;
    }

}
