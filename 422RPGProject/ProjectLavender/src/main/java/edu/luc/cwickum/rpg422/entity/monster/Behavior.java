package edu.luc.cwickum.rpg422.entity.monster;

import edu.luc.cwickum.rpg422.entity.Spell;

/**
 * Created by Cooper Wickum on 10/28/13.
 */
public class Behavior {

    private Integer priority;
    private Spell spell;

    public Behavior (Integer pri, Spell spell) {
        this.priority = pri;
        this.spell = spell;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Spell getSpell() {
        return spell;
    }

    public void setSpell(Spell spell) {
        this.spell = spell;
    }

}
