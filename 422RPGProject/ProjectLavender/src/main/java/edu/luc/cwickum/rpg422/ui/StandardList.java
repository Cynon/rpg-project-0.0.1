package edu.luc.cwickum.rpg422.ui;

/**
 * Created by Cooper on 12/2/13.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import edu.luc.cwickum.rpg422.R;

public class StandardList extends BaseAdapter {

    Context context;
    String[] mName;
    LayoutInflater inflater;

    public StandardList(Context context, String[] title) {
        this.context = context;
        this.mName = title;
    }

    @Override
    public int getCount() {
        return mName.length;
    }

    @Override
    public Object getItem(int position) {
        return mName[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        TextView txtName;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.standard_list, parent,
                false);

        txtName = (TextView) itemView.findViewById(R.id.name);

        txtName.setText(mName[position]);

        return itemView;
    }

}

