package edu.luc.cwickum.rpg422.item;

import java.util.ArrayList;
import java.util.HashMap;

import edu.luc.cwickum.rpg422.global.DamageTypes;
import edu.luc.cwickum.rpg422.global.EffectTypes;
import edu.luc.cwickum.rpg422.global.EquipmentType;

/**
 * Created by Cooper Wickum on 11/5/13.
 */
public class ItemEquip extends Item {

    //ItemEquip class refers to equipment, not consumables.
    protected HashMap<String, Integer> boost; //Stat boosts.
    protected HashMap<String, Integer> elementsMap; //Elemental resistances by percentage.
                                                    // Negative numbers indicate that receiving that kind of damage heals you. 0 indicates no damage. Normal damage is 100.
    protected ArrayList<String> elements; //Element

    protected HashMap<String, Integer> badStatusMap; //Bad status resistances by percentage.
    protected ArrayList<String> badStatus; //Bad status resistances that exist.

    protected String name; //Item name
    protected String desc; //Short description.

    protected String equipType; //Equipment type.

    protected ItemEquip() {

        id = -1;
        name = "ITEMNAME";
        desc = "...";
        boost = new HashMap<String, Integer>();
        setBoost(0, 0, 0, 0, 0, 0);
        consumable = false;

        elementsMap = new HashMap<String, Integer>();
        elements = new ArrayList<String>();

        badStatusMap = new HashMap<String, Integer>();
        badStatus = new ArrayList<String>();


    }

    public ItemEquip(String eqType, int idnum) {

      id = idnum;
      equipType = EquipmentType.valueOf(eqType).toString();

      name = "ITEMNAME";
      desc = "...";
      boost = new HashMap<String, Integer>();
      elementsMap = new HashMap<String, Integer>();
      setBoost(0, 0, 0, 0, 0, 0);
      consumable = false;


    }

    //Set the stats of an entity.
    private void setBoost(Integer ad, Integer df, Integer mg, Integer mr, Integer sp, Integer lk)
    {
        boost.put("attack", ad);
        boost.put("defense", df);
        boost.put("magic", mg);
        boost.put("magicRes", mr);
        boost.put("speed", sp);
        boost.put("luck", lk);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public HashMap<String, Integer> getBoost() {
        return boost;
    }

    public boolean addTypeRes(String s, Integer pct)
    {
        //Add a resistance/weakness for the item.
        //Remember that negative numbers indicate that receiving that kind of damage heals you.
        //0 indicates no damage. Normal damage is 100 -- if information is not available,
        //the damage will be assumed to be 100.

        System.out.println(s);
        //Make sure you don't put in a bad type or anything like that...
        s = DamageTypes.valueOf(s).toString();
        System.out.println(s);

        int a = 0;
        while (a < elements.size())
        {
            if (elements.get(a).equals(s))
            {
                a = elements.size();
                return false;
            }

            a++;
        }
        elements.add(s);
        elementsMap.put(s,pct);
        return true;
    }

    public boolean removeTypeRes(String s){

        s = DamageTypes.valueOf(s).toString();

        if (elements.contains(s))
        {
            elements.remove(elements.indexOf(s));
            elementsMap.remove(s);
            return true;
        }
        else return false;
    }

    public boolean addEffectRes(String s, Integer pct)
    {
        //Add a bad status resistance for an item.

        System.out.println(s);
        //Make sure you don't put in a bad type or anything like that...
        s = EffectTypes.valueOf(s).toString();
        System.out.println(s);

        if (pct > 100)
        {
            pct = 100;
        }
        else if (pct < 0)
        {
            pct = 0;
        }

        int a = 0;
        while (a < badStatus.size())
        {
            if (badStatus.get(a).equals(s))
            {
                a = badStatus.size();
                return false;
            }

            a++;
        }
        badStatus.add(s);
        badStatusMap.put(s,pct);
        return true;
    }

    public boolean removeEffectRes(String s){

        s = EffectTypes.valueOf(s).toString();

        if (badStatus.contains(s))
        {
            badStatus.remove(badStatus.indexOf(s));
            badStatusMap.remove(s);
            return true;
        }
        else return false;
    }

}
