package edu.luc.cwickum.rpg422.entity;

import java.util.ArrayList;

import edu.luc.cwickum.rpg422.item.Item;

/**
 * Created by Cooper on 11/5/13.
 */

public class SpellFactory {

public static Spell getSpell(Integer i)
{

    if (i < 0) { return null; }

    /* Ideally, spells 0-9 will be for special cases.
     *
     * Spell 1 was intended to be a Steal function (steal item from enemy), and
     * Spell 2 was supposed to be Scan (see the enemy's health), but those were not
     * implemented in time.
     *
     */

    else if (i == 0) {
        Spell s = new Spell(0);
        s.setName("AI ATTACK");
        s.setDesc("Spell #0 is used for the AI's regular attacks. Go away.");
        s.setAccuracy(85);

        return s;
    }


    else if (i == 10) {
        Spell s = new Spell(10);
        s.setName("Fireball");
        s.setDesc("Goodness gracious!");
        s.setBaseDamage(30);
        s.setMagic(true);
        s.setMpCost(0);
        ArrayList<String> elementDamage = new ArrayList<String>();
        elementDamage.add("fire");


        return s;
    }

    else if (i == 11) {
        Spell s = new Spell(11);
        s.setName("Snowball");
        s.setDesc("Snowball fight!!!");
        s.setBaseDamage(30);
        s.setMagic(true);
        s.setMpCost(0);
        ArrayList<String> elementDamage = new ArrayList<String>();
        elementDamage.add("ice");


        return s;
    }

    else if (i == 12) {
        Spell s = new Spell(12);
        s.setName("Lightning Beam");
        s.setDesc("A beam of lightning. How the hell does that work?");
        s.setBaseDamage(30);
        s.setMagic(true);
        s.setMpCost(0);
        ArrayList<String> elementDamage = new ArrayList<String>();
        elementDamage.add("lightning");


        return s;
    }

    else if (i == 15) {
        Spell s = new Spell(15);
        s.setName("Heal Force");
        s.setDesc("Restores a small amount of HP.");
        s.setBaseDamage(30);
        s.setHealing(true);
        s.setMpCost(0);

        return s;
    }

    else if (i == 25) {
        Spell s = new Spell(25);
        s.setName("Potion");
        s.setDesc("Restores 60 HP.");
        s.setBaseDamage(60);
        s.setHealing(true);
        s.setMagic(false);
        s.setTrueDamage(true);
        s.setMpCost(0);

        return s;
    }


    return null;
}

}


