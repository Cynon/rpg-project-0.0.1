package edu.luc.cwickum.rpg422;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import edu.luc.cwickum.rpg422.battle.BattleActivity;
import edu.luc.cwickum.rpg422.entity.*;
import edu.luc.cwickum.rpg422.entity.Character;
import edu.luc.cwickum.rpg422.global.CharacterClasses;
import edu.luc.cwickum.rpg422.global.DefaultCharacterNames;
import edu.luc.cwickum.rpg422.ui.StandardList;
import edu.luc.cwickum.rpg422.ui.StatsList;

public class CharacterSelectActivity extends Activity {

    ListView mClassList;
    ListView mStatList;
    TextView mClassName;
    TextView mClassDesc;
    String[] mClassListContents;
    String[] mStatName;
    int[] mStat;
    int[] mStatGrowth;
    int[] mStatIcon;
    String[] mDescription;
    private StandardList mClassListAdapter;
    private StatsList mStatListAdapter;
    int pos;
    protected Party mParty;
    protected ArrayList<Character> mPartyMembers;
    private EditText mNameInput;
    Button mSelectClass;

    public CharacterSelectActivity()
    {
        mPartyMembers = new ArrayList<Character>();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);


        mClassList = (ListView) findViewById(R.id.listView_class);
        mStatList = (ListView) findViewById(R.id.listView_baseStats);
        mClassName = (TextView) findViewById(R.id.className);
        mClassDesc = (TextView) findViewById(R.id.classDesc);
        mNameInput = (EditText) findViewById(R.id.inputName);
        mSelectClass = (Button) findViewById(R.id.selectClass);


        mClassListContents = new String[] {
                "Warrior",
                "Soldier",
                "Assassin",
                "Sorcerer"
        };

        mDescription = new String[] {
                "Strong physical attackers, however, they are slow.",
                "Strong defense and physical attack.",
                "Appears weak, but has a greater critical hit chance.",
                "Magic wielders who specialize in magic damage. Comes with two spells."
        };

        mStatName = new String[] {
                "Health",
                "Mana",
                "Attack",
                "Defense",
                "Magic Power",
                "Magic Def.",
                "Agility",
                "Luck"
        };

        mStatIcon = new int[] {
                0,
                0,
                R.drawable.icon_stat_attack,
                R.drawable.icon_stat_defense,
                R.drawable.icon_stat_magic,
                R.drawable.icon_stat_magicres,
                R.drawable.icon_stat_speed,
                R.drawable.icon_stat_luck
        };


        mClassListAdapter = new StandardList(getBaseContext(),
                mClassListContents);
        mClassList.setAdapter(mClassListAdapter);
        mClassList.setOnItemClickListener(new ListItemClickListener());


        findViewById(R.id.selectClass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (createCharacter())
                {
                    if (mPartyMembers.size() == 2)
                    {
                        mSelectClass.setText("Begin Game");
                    }
                    else if (mPartyMembers.size() >= 3)
                    {
                        mParty = new Party(mPartyMembers);
                        mParty.fullHealAll();
                        startGame();
                    }
                    else
                    {
                        mSelectClass.setText("Select Class");
                    }
                }
                else
                {
                    if (mPartyMembers.size() >= 3)
                    {
                        mParty = new Party(mPartyMembers);
                        startGame();
                    }
                    else
                    {
                        Toast.makeText(
                               getBaseContext(),
                               "Something went wrong here. I have no idea how you managed to break it, but you did... congratulations?",
                               Toast.LENGTH_SHORT).show();
                    }
                }

                mNameInput.setText(DefaultCharacterNames.getName(mPartyMembers.size()));
            }
        });

        findViewById(R.id.selectBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mPartyMembers.size() !=0)
                {

                    Character c = mPartyMembers.get(mPartyMembers.size() - 1);
                    mPartyMembers.remove(mPartyMembers.size() - 1);
                    mSelectClass.setText("Select Class");

                    Toast.makeText(
                            getBaseContext(),
                            c.getName() + " left the party!",
                            Toast.LENGTH_SHORT).show();

                    mNameInput.setText(c.getName());

                }
                else
                {
                    Toast.makeText(
                            getBaseContext(),
                            "You have nobody in your party!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void startGame()
    {
        Intent in = new Intent(this, BattleActivity.class);
        in.putExtra("PARTY", mParty);
        startActivity(in);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        gl.glViewport(0, 0, width, height);

        // make adjustments for screen ratio
        float ratio = (float) width / height;
        gl.glMatrixMode(GL10.GL_PROJECTION);        // set matrix to projection mode
        gl.glLoadIdentity();                        // reset the matrix to its default state
        gl.glFrustumf(-ratio, ratio, -1, 1, 3, 7);  // apply the projection matrix
    }


    private class ListItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            selectItem(position);
            pos = position;


            mStat = new int[] {
                    CharacterClasses.getCharacterClass(position).get("maxHP"),
                    CharacterClasses.getCharacterClass(position).get("maxMP"),
                    CharacterClasses.getCharacterClass(position).get("attack"),
                    CharacterClasses.getCharacterClass(position).get("defense"),
                    CharacterClasses.getCharacterClass(position).get("magic"),
                    CharacterClasses.getCharacterClass(position).get("magicRes"),
                    CharacterClasses.getCharacterClass(position).get("speed"),
                    CharacterClasses.getCharacterClass(position).get("luck")
            };

            mStatGrowth = new int[] {
                    CharacterClasses.getCharacterClass(pos).get("GmaxHP"),
                    CharacterClasses.getCharacterClass(pos).get("GmaxMP"),
                    CharacterClasses.getCharacterClass(pos).get("Gattack"),
                    CharacterClasses.getCharacterClass(pos).get("Gdefense"),
                    CharacterClasses.getCharacterClass(pos).get("Gmagic"),
                    CharacterClasses.getCharacterClass(pos).get("GmagicRes"),
                    CharacterClasses.getCharacterClass(pos).get("Gspeed"),
                    CharacterClasses.getCharacterClass(pos).get("Gluck")
            };

            mStatListAdapter = new StatsList(getBaseContext(),
                    mStatIcon, mStatName, mStat, mStatGrowth);
            mStatList.setAdapter(mStatListAdapter);
            //mStatList.setOnItemClickListener(new ListItemClickListener());

        }
    }

    private void selectItem(int position) {

        mClassName.setText(mClassListContents[position].toString());
        mClassDesc.setText(mDescription[position].toString());

        //mStatList.getAdapter(mStatListAdapter.getView())

        //mStatListAdapter.getView(position, mStatListAdapter, mStatList);

    }

    private boolean createCharacter() {

        if (mPartyMembers.size() == 3)
        {
            return false;
        }
        else
        {
            Character c = new Character();

            c.setName(mNameInput.getText().toString());
            mNameInput.clearComposingText();

            c.setStats(CharacterClasses.getCharacterClass(pos).get("maxHP"),
                    CharacterClasses.getCharacterClass(pos).get("maxMP"),
                    CharacterClasses.getCharacterClass(pos).get("attack"),
                    CharacterClasses.getCharacterClass(pos).get("defense"),
                    CharacterClasses.getCharacterClass(pos).get("magic"),
                    CharacterClasses.getCharacterClass(pos).get("magicRes"),
                    CharacterClasses.getCharacterClass(pos).get("speed"),
                    CharacterClasses.getCharacterClass(pos).get("luck"));
            c.setGrowthRate(CharacterClasses.getCharacterClass(pos).get("GmaxHP"),
                    CharacterClasses.getCharacterClass(pos).get("GmaxMP"),
                    CharacterClasses.getCharacterClass(pos).get("Gattack"),
                    CharacterClasses.getCharacterClass(pos).get("Gdefense"),
                    CharacterClasses.getCharacterClass(pos).get("Gmagic"),
                    CharacterClasses.getCharacterClass(pos).get("GmagicRes"),
                    CharacterClasses.getCharacterClass(pos).get("Gspeed"),
                    CharacterClasses.getCharacterClass(pos).get("Gluck"));
            c.setCritChance(CharacterClasses.getCharacterClass(pos).get("critical"));


            //Give Sorcerer spells.
            if (pos == 3) {
                ArrayList<Spell> s = new ArrayList<Spell>();
                s.add(SpellFactory.getSpell(10));
                s.add(SpellFactory.getSpell(11));
                c.setSpells(s);
            }

            //For demo purposes only.
            c.levelUp();

            mPartyMembers.add(c);

            Toast.makeText(
                    getBaseContext(),
                    mNameInput.getText().toString() + " joined the party!",
                    Toast.LENGTH_SHORT).show();


            return true;
        }


    }


}
