package edu.luc.cwickum.rpg422.maps;

/**
 * Created by Cooper Wickum on 12/3/13.
 *
 * Not implemented yet. Intended to be used for the tiles characters walk around
 * the overworld with. Anyone wishing to contribute should email me at cynon777@gmail.com
 * with the subject line as "Project Lavender". I will be more than happy to work with you.
 *
 */
public class Tile {

    boolean walkNorth;
    boolean walkSouth;
    boolean walkEast;
    boolean walkWest;
    int layer; // Layer 0 = below the player (player graphic renders on top of it),
               // Layer 1 = same layer,
               // Layer 2 = Above the player (like tree branches),
               // Layer 3 = above layer 2.
    int tileID; //For a TileFactory to use.
    int tileImage; //Should be a SQUARE image. Preferably, tiles should be 16x16 to comply with
                   //the size of tiles in the types of games this is homaging.

    public Tile()
    {

    }

}
