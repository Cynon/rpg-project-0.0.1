package edu.luc.cwickum.rpg422.global;

import java.util.HashMap;

/**
 * Created by Cooper on 12/6/13.
 */
public class CharacterClasses {

    public static HashMap<String, Integer> getCharacterClass(int a) {
        HashMap<String, Integer> stats = new HashMap<String, Integer>();

        if (a == 0)
        {

            stats.put("maxHP", 101);
            stats.put("maxMP", 12);
            stats.put("attack", 41);
            stats.put("defense", 31);
            stats.put("magic", 26);
            stats.put("magicRes", 22);
            stats.put("speed", 19);
            stats.put("luck", 2);
            stats.put("GmaxHP", 15);
            stats.put("GmaxMP", 3);
            stats.put("Gattack", 8);
            stats.put("Gdefense", 5);
            stats.put("Gmagic", 4);
            stats.put("GmagicRes", 4);
            stats.put("Gspeed", 5);
            stats.put("Gluck", 1);
            stats.put("critical", 8);

        }

        //Base Stats -- Soldier
        else if (a == 1)
        {

            stats.put("maxHP", 115);
            stats.put("maxMP", 17);
            stats.put("attack", 46);
            stats.put("defense", 31);
            stats.put("magic", 29);
            stats.put("magicRes", 25);
            stats.put("speed", 25);
            stats.put("luck", 3);
            stats.put("GmaxHP", 18);
            stats.put("GmaxMP", 4);
            stats.put("Gattack", 6);
            stats.put("Gdefense", 7);
            stats.put("Gmagic", 3);
            stats.put("GmagicRes", 4);
            stats.put("Gspeed", 6);
            stats.put("Gluck", 1);
            stats.put("critical", 10);

        }

        //Base Stats -- Assassin
        else if (a == 2)
        {

            stats.put("maxHP", 93);
            stats.put("maxMP", 17);
            stats.put("attack", 46);
            stats.put("defense", 29);
            stats.put("magic", 17);
            stats.put("magicRes", 19);
            stats.put("speed", 32);
            stats.put("luck", 5);
            stats.put("GmaxHP", 11);
            stats.put("GmaxMP", 3);
            stats.put("Gattack", 6);
            stats.put("Gdefense", 7);
            stats.put("Gmagic", 3);
            stats.put("GmagicRes", 4);
            stats.put("Gspeed", 6);
            stats.put("Gluck", 2);
            stats.put("critical", 15);

        }

        //Base Stats -- Sorcerer
        else if (a == 3)
        {

            stats.put("maxHP", 84);
            stats.put("maxMP", 39);
            stats.put("attack", 23);
            stats.put("defense", 27);
            stats.put("magic", 40);
            stats.put("magicRes", 38);
            stats.put("speed", 16);
            stats.put("luck", 1);
            stats.put("GmaxHP", 8);
            stats.put("GmaxMP", 8);
            stats.put("Gattack", 3);
            stats.put("Gdefense", 4);
            stats.put("Gmagic", 9);
            stats.put("GmagicRes", 7);
            stats.put("Gspeed", 5);
            stats.put("Gluck", 1);
            stats.put("critical", 8);

        }


        return stats;
    }

}
