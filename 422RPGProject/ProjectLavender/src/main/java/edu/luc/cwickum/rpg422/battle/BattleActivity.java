package edu.luc.cwickum.rpg422.battle;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import javax.microedition.khronos.opengles.GL10;

import edu.luc.cwickum.rpg422.R;
import edu.luc.cwickum.rpg422.entity.*;
import edu.luc.cwickum.rpg422.entity.Character;
import edu.luc.cwickum.rpg422.entity.monster.*;
import edu.luc.cwickum.rpg422.ui.BattleHeroList;
import edu.luc.cwickum.rpg422.ui.StandardList;

public class BattleActivity extends Activity {


    Party mParty;
    ActionTimer actionTimer;
    ArrayList<Monster> mEnemy;
    private String[] mEnemyName;
    private String[] mCommands;
    private String[] mHeroName;
    int[] mHP;
    int[] mMP;
    int[] mBadStatusIcon; //Unused.
    private ListView mHeroList;
    private ListView mCommandsList;
    private ListView mEnemyList;
    private TextView mHeroNameTV;
    private TextView mHPTV;
    private BattleHeroList mHeroListAdapter;
    private StandardList mEnemyListAdapter;
    private StandardList mCommandListAdapter;
    private TextView mMPTV;
    boolean canAct;

    int enemyTarget;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.battlemenu);
        Intent intent = getIntent();
        mParty = (Party) intent.getParcelableExtra("PARTY");

        //Static encounter used for this demo. In the future, that will be parcelable as well. -- Cooper
        mEnemy = EncounterFactory.encounterMonster(10);

        actionTimer = new ActionTimer(mParty, mEnemy);
        canAct = true;

        mHeroList = (ListView) findViewById(R.id.listView_hero);
        mCommandsList = (ListView) findViewById(R.id.listView_commands);
        mEnemyList = (ListView) findViewById(R.id.listView_monsterList);
        mHeroNameTV = (TextView) findViewById(R.id.charName);
        mHPTV = (TextView) findViewById(R.id.charHP);
        mMPTV = (TextView) findViewById(R.id.charMP);

        heroListUpdate();
        enemyListUpdate();

        if (canAct) { checkATB(); }
    }


    //Update HP and MP
    public void heroListUpdate()
    {
        mHeroName = new String[] {
                mParty.getCharBySlot(0).getName(),
                mParty.getCharBySlot(1).getName(),
                mParty.getCharBySlot(2).getName()
        };
        //Not yet implemented
        mBadStatusIcon = new int[] {
                0,
                0,
                0
        };
        mHP = new int[] {
                mParty.getCharBySlot(0).getHealth(),
                mParty.getCharBySlot(1).getHealth(),
                mParty.getCharBySlot(2).getHealth()
        };
        mMP = new int[] {
                mParty.getCharBySlot(0).getMana(),
                mParty.getCharBySlot(1).getMana(),
                mParty.getCharBySlot(2).getMana()
        };

        mHeroListAdapter = new BattleHeroList(getBaseContext(),
                mBadStatusIcon, mHeroName, mHP, mMP);
        mHeroList.setAdapter(mHeroListAdapter);
        mHeroList.setClickable(false);

    }


    public void enemyListUpdate()
    {
        //Makes sure you can't hit an already dead monster.
        int a = 0;
        ArrayList<String> nameList = new ArrayList<String>();
        while (a < mEnemy.size())
        {
            if (!mEnemy.get(a).isAlive())
            {
                mEnemy.remove(a);
            }
            else
            {
                nameList.add(mEnemy.get(a).getName());
                a++;
            }

            mEnemyName = nameList.toArray(new String[nameList.size()]);

            mEnemyListAdapter = new StandardList(getBaseContext(),
                    mEnemyName);
            mEnemyList.setAdapter(mEnemyListAdapter);

            mEnemyList.setOnItemClickListener(new EnemyLastItemClickListener());

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        gl.glViewport(0, 0, width, height);

        // make adjustments for screen ratio
        float ratio = (float) width / height;
        gl.glMatrixMode(GL10.GL_PROJECTION);        // set matrix to projection mode
        gl.glLoadIdentity();                        // reset the matrix to its default state
        gl.glFrustumf(-ratio, ratio, -1, 1, 3, 7);  // apply the projection matrix
    }

    public boolean allEnemyDead()
    {
        int a = 0;
        while (a < mEnemy.size())
        {
            if (mEnemy.get(a).isAlive())
            {
                return false;
            }
            a++;
        }
        return true;
    }

    public boolean allCharactersDead()
    {
        int a = 0;
        while (a < mParty.size())
        {
            if (mParty.getCharBySlot(a).isAlive())
            {
                return false;
            }
            a++;
        }
        return true;
    }


    public void checkATB()
    {
        System.out.println(actionTimer.getActive().size() + " CAN ACT.");

        while (canAct) {

        if (allEnemyDead())
        {

        }

        Ticker atb = new Ticker();
        atb.execute();

            try {
                commense(atb.get());
                heroListUpdate();
                enemyListUpdate();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }

    }

    public void commense(Entity e)
    {
        System.out.println("ATTACK PHASE");

        canAct = false;
        System.out.println("COMMENSE");
        //Entity e = actionTimer.nextAction();
        System.out.println("TURN FOR: " + e.getName());

        if (e instanceof Character)
        {

            Character c = (Character) e;

            System.out.println("HERO " + e.getName() + " ACTING");

            //This will DEFINITELY get fixed for future releases.
            mCommands = new String[] {
                    "Attack",
                    "Magic"
            };

            mCommandListAdapter = new StandardList(getBaseContext(),
                    mCommands);
            mCommandsList.setAdapter(mCommandListAdapter);

            ListItemClickListener selector = new ListItemClickListener();
            selector.setCharacter(c);
            mCommandsList.setClickable(true);

            mCommandsList.setOnItemClickListener(selector);

        }
        else if (e instanceof Monster)
        {
            System.out.println("MONSTER " + e.getName() + " ACTING");
            Monster m = (Monster) e;
            Spell action = m.monsterAIaction();
            System.out.println("MONSTER " + e.getName() + " CASTS SPELL " + action.getId() + " " + action.getName());
            Character target = getAITarget();
            AttackMessage am = MagicDamage.MagicDamageFormula(m, target, action);

            Toast.makeText(
                    getBaseContext(),
                    target.getName() + " took " + am.getDamage() + " from " + m.getName(),
                    Toast.LENGTH_SHORT).show();

            target.hit(am.getDamage());

            if (!target.isAlive())
            {
                Toast.makeText(
                        getBaseContext(),
                        target.getName() + " was slain!",
                        Toast.LENGTH_SHORT).show();
            }

            canAct = true;
            checkATB();


        }

    }

    public Character getAITarget()
    {
        Random r = new Random(444);

        boolean selected = false;

        while (!selected)
        {
            int a = r.nextInt(3);
            a--;

            if (mParty.getCharBySlot(a).isAlive())
            {
                selected = true;
                System.out.println("Targeting " + mParty.getCharBySlot(a).getName());
                return mParty.getCharBySlot(a);
            }
        }

        return null;

    }

    private class ListItemClickListener implements
            ListView.OnItemClickListener {

        Character c;

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {



            AttackMessage am = new AttackMessage(0, false, false);

            if (position == 0)
            {
                am = AttackDamage.AttackDamageFormula(c, mEnemy.get(enemyTarget));

                Toast.makeText(
                        getBaseContext(),
                        mEnemy.get(enemyTarget).getName() + " took " + am.getDamage() + " from " + c.getName(),
                        Toast.LENGTH_SHORT).show();

                mEnemy.get(enemyTarget).hit(am.getDamage());
                System.out.println(mEnemy.get(enemyTarget).getHealth() + " " + mEnemy.get(enemyTarget).isAlive());

                if (!mEnemy.get(enemyTarget).isAlive())
                {
                    Toast.makeText(
                            getBaseContext(),
                            mEnemy.get(enemyTarget).getName() + " was slain!",
                            Toast.LENGTH_SHORT).show();
                }


                endTurn();

            }
            else if (position == 1)
            {


                endTurn();

            }

        }

        public void setCharacter(Character c)
        {
            this.c = c;
        }

        public void endTurn()
        {

            mCommandsList.clearFocus();
            mEnemyList.clearFocus();

            mCommands = new String[] {
                    "",
                    ""
            };

            mCommandListAdapter = new StandardList(getBaseContext(),
                    mCommands);
            mCommandsList.setAdapter(mCommandListAdapter);
            mCommandsList.setClickable(false);

            canAct = true;
            checkATB();
        }
    }

    private class EnemyLastItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {

            Toast.makeText(
                    getBaseContext(),
                    "Targeting " + mEnemy.get(position).getName(),
                    Toast.LENGTH_SHORT).show();

            if (mEnemy.get(position).isAlive())
            { enemyTarget = position; }
            else
            {
                mEnemyList.clearFocus();
            }

        }
    }

    public class Ticker extends AsyncTask<Void, Void, Entity>
    {
        @Override
        protected Entity doInBackground(Void... voids) {

            while (actionTimer.getActive().size() == 0)
            {
                actionTimer.addTick();
            }

            Entity e = actionTimer.nextAction();
            System.out.println(e.getName());
            return e;
        }

    }



}
