package edu.luc.cwickum.rpg422.item;

import edu.luc.cwickum.rpg422.entity.Spell;

/**
 * Created by Cooper Wickum on 11/5/13.
 */

//Consumable items include potions and grenades.

public class ItemConsumable extends Item {

    private Spell itemEffect;

    public ItemConsumable(Spell spell) {

        itemEffect = spell;
        consumable = true;
        name = spell.getName();

    }

    public boolean use()
    {
        //Spell.cast();
        return false;
    }

}
