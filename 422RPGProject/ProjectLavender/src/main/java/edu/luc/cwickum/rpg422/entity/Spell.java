package edu.luc.cwickum.rpg422.entity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Cooper Wickum on 10/28/13.
 */
public class Spell {

    private String type; //Type of attack.
    private Integer id; //Used for sorting. Anything less than 1 cannot be obtained by the player. Spell #0 is used for the AI's regular attacks.
    private String name; //Spell name
    private String desc; //Description of the spell.
    private Integer baseDamage; //Base damage. For healing spells, used as the base healing.
    private boolean trueDamage; //If the spell does true damage, the base damage is not affected by anything, such as defense, magic resistance, magic power.
                                //Setting isMagic to false and isHealing to true will allow for this to be used as a potion.
    private Integer mpCost; //Cost in MP. Can not be negative.
    private boolean isMagic; //If true, does magic damage. If false, does physical damage.
    private boolean targetAll; //Targets all opponents.
    private boolean isHealing; //Spell heals the target instead of damaging them.
    private HashMap<String,Integer> effectsMap; //Chances of each status happening, with 100 being 100% chance of the effect happening.
                                                // For healing spells, this is the chance that the status will be removed if it is found.
    private ArrayList<String> effects; //Status effects that this spell might cause, or in the case of healing spells, remove.
    private ArrayList<String> elements; //Elemental damage this spell causes.

    private Integer accuracy; //Spell accuracy. Defaults to 100 if not set. Can not be higher than 100 or lower than 1.

    public Spell(Integer idd) {

        this.id = idd;
        this.name = "SPELLNAME";
        this.desc = "...";
        this.baseDamage = 0;
        this.mpCost = 0;
        this.isMagic = true;
        this.targetAll = false;
        this.isHealing = false;
        this.elements = new ArrayList<String>();
        this.accuracy = 100;


    }

    public Spell(Integer idd, String name, Integer dmg, Integer cost, ArrayList<String> damageList)
    {
        this.id = idd;
        this.name = name;
        this.baseDamage = dmg;
        this.mpCost = cost;
        this.elements = damageList;
        this.isMagic = true;
        this.targetAll = false;
        this.isHealing = false;
        this.accuracy = 100;
    }

    public Integer getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBaseDamage() {
        return baseDamage;
    }

    public boolean setBaseDamage(Integer baseDamage) {

        if (baseDamage >= 0) {

            this.baseDamage = baseDamage;
            return true;
        }
        else return false;
    }

    public boolean isMagic() {
        return isMagic;
    }

    public void setMagic(boolean isMagic) {
        this.isMagic = isMagic;
    }

    public boolean isTargetAll() {
        return targetAll;
    }

    public void setTargetAll(boolean targetAll) {
        this.targetAll = targetAll;
    }

    public boolean isHealing() {
        return isHealing;
    }

    public void setHealing(boolean isHealing) {
        this.isHealing = isHealing;
    }

    public ArrayList<String> getElements() {
        return elements;
    }

    public boolean addElement(String s)
    {
        //Add a damage type for this.

        System.out.println(s);
        //Make sure you don't put in a bad type or anything like that...
        s = DamageTypes.valueOf(s).toString();
        System.out.println(s);

        int a = 0;
        while (a < elements.size())
        {
            if (elements.get(a).equals(s))
            {
                a = elements.size();
                return false;
            }

            a++;
        }
        elements.add(s);
        return true;
    }

    //Remove an elemental damage type.
    public boolean removeElement(String s){

        s = DamageTypes.valueOf(s).toString();

        if (elements.contains(s))
        {
            elements.remove(elements.indexOf(s));
            return true;
        }
        else return false;
    }

    public boolean addEffect(String s, Integer pct)
    {
        //Add a bad status effect for this spell.

        System.out.println(s);
        //Make sure you don't put in a bad type or anything like that...
        s = EffectTypes.valueOf(s).toString();
        System.out.println(s);

        //Integer pct is the chance of the spell carrying that bad effect, or in the case
        //of a healing spell, remove the effect. PCT can never be higher than 100 or lower than 0.

        if (pct > 100)
        {
            pct = 100;
        }
        else if (pct < 0)
        {
            pct = 0;
        }

        int a = 0;
        while (a < effects.size())
        {
            if (effects.get(a).equals(s))
            {
                a = effects.size();
                return false;
            }

            a++;
        }
        effects.add(s);
        effectsMap.put(s,pct);
        return true;
    }

    public boolean removeEffect(String s){

        s = EffectTypes.valueOf(s).toString();

        if (effects.contains(s))
        {
            effects.remove(effects.indexOf(s));
            effectsMap.remove(s);
            return true;
        }
        else return false;
    }


    public Integer getMpCost() {
        return mpCost;
    }

    public void setMpCost(Integer mpCost) {

        //This should not ever be less than 0. If that was allowed, you would regain MP for casting
        //spells, which is NOT the idea.
        this.mpCost = mpCost;
    }

    public HashMap<String, Integer> getEffectsMap() {
        return effectsMap;
    }

    public ArrayList<String> getEffects() {
        return effects;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    //If the spell does true damage, the base damage is not affected by anything, such as defense, magic resistance, magic power.
    //Setting isMagic to false and isHealing to true will allow for this to be used as a potion.
    public boolean isTrueDamage() {
        return trueDamage;
    }

    public void setTrueDamage(boolean trueDamage) {
        this.trueDamage = trueDamage;
    }

    public void setAccuracy(Integer accuracy) {

        if (accuracy > 100) {
            accuracy = 100;
        }
        else if (accuracy < 0) {
            accuracy = 0;
        }
        this.accuracy = accuracy;
    }

    public int getAccuracy()
    {
        return this.accuracy;
    }

    //All of these are elemental damage types. The last 4 are intended for physical attacks only.
    public enum DamageTypes {
        fire, ice, lightning, water, earth, wind, light, dark, slashing, crushing, piercing, ranged
    }


    //Bad statuses. These may or may not be implemented in time...
    public enum EffectTypes {
        poison, stun, sleep, silence, death
    }

}

