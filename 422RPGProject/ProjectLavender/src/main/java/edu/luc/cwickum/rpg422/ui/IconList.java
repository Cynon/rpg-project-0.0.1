package edu.luc.cwickum.rpg422.ui;

/**
 * Created by Cooper on 12/2/13.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import edu.luc.cwickum.rpg422.R;

public class IconList extends BaseAdapter {

    Context context;
    String[] mName;
    int[] mIcon;
    LayoutInflater inflater;

    public IconList(Context context, String[] title, int[] icon) {
        this.context = context;
        this.mName = title;
        this.mIcon = icon;
    }

    @Override
    public int getCount() {
        return mName.length;
    }

    @Override
    public Object getItem(int position) {
        return mName[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        TextView txtName;
        ImageView imgIcon;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.icon_list, parent,
                false);

        txtName = (TextView) itemView.findViewById(R.id.name);
        imgIcon = (ImageView) itemView.findViewById(R.id.icon);

        txtName.setText(mName[position]);
        imgIcon.setImageResource(mIcon[position]);

        return itemView;
    }

}

